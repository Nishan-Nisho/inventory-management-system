<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BoxController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\StoreController;
use App\Http\Controllers\VendorController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\FrontendController;

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

Route::get('/dashboard', function () {
    return view('backend.dashboard');
})->middleware(['auth'])->name('dashboard');

Route::get('/box', function () {
    return view('backend.boxview');
})->middleware(['auth'])->name('boxview');

Route::get('/inventory', function () {
    return view('backend.inventory');
})->name('inventory');



// Route::get('/dashboard', function () {
//     return view('dashboard');
// })->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';





Route::prefix('categories')->group(function(){
    Route::get('/',[CategoryController::class,'index'])->name('categories.index');
    Route::get('/create',[CategoryController::class,'create'])->name('categories.create');
    Route::post('/store',[CategoryController::class,'store'])->name('categories.store');
    Route::get('/show/{id}',[CategoryController::class,'show'])->name('categories.show');
    Route::get('/edit/{id}',[CategoryController::class,'edit'])->name('categories.edit');
    Route::post('/update{id}',[CategoryController::class,'update'])->name('categories.update');
    Route::get('/delete{id}',[CategoryController::class,'delete'])->name('categories.delete');
    Route::get('/trashlist',[CategoryController::class,'trashlist'])->name('categories.trashlist');
    Route::get('/restore/{id}',[CategoryController::class,'restore'])->name('categories.restore');
    Route::get('/permanentdelete/{id}',[CategoryController::class,'permanentDelete'])->name('categories.permanentdelete');
});


//customer

Route::prefix('customers')->group(function () {
    Route::get('/',[CustomerController::class,'index'])->name('customer.index');
    Route::get('/create',[CustomerController::class,'create'])->name('customer.create');
    Route::post('/store',[CustomerController::class,'store'])->name('customer.store');
    Route::get('/show/{id}',[CustomerController::class,'show'])->name('customer.show');
    Route::get('/edit/{id}',[CustomerController::class,'edit'])->name('customer.edit');
    Route::post('/update/{id}',[CustomerController::class,'update'])->name('customer.update');
    Route::get('/delete/{id}',[CustomerController::class,'destroy'])->name('customer.delete');
    Route::get('/trashlist',[CustomerController::class,'trashlist'])->name('customer.trashlist');
    Route::get('/restore/{id}',[CustomerController::class,'restore'])->name('customer.restore');
    Route::get('/permanentdelete/{id}',[CustomerController::class,'permanentDelete'])->name('customer.permanentdelete');

});

Route::prefix('products')->group(function(){
    Route::get('/',[ProductController::class,'index'])->name('products.index');
    Route::get('/create',[ProductController::class,'create'])->name('products.create');
    Route::post('/store',[ProductController::class,'store'])->name('products.store');
    Route::get('/show/{id}',[ProductController::class,'show'])->name('products.show');
    Route::get('/edit/{id}',[ProductController::class,'edit'])->name('products.edit');
    Route::post('/update{id}',[ProductController::class,'update'])->name('products.update');
    Route::get('/delete{id}',[ProductController::class,'delete'])->name('products.delete');
    // Route::get('/trashlist',[ProductsController::class,'trashlist'])->name('products.trashlist');
    // Route::get('/restore/{id}',[ProductsController::class,'restore'])->name('products.restore');
    // Route::get('/permanentdelete/{id}',[ProductsController::class,'permanentDelete'])->name('products.permanentdelete');
});

Route::prefix('vendors')->group(function(){
    Route::get('/',[VendorController::class,'index'])->name('vendors.index');
    Route::get('/create',[VendorController::class,'create'])->name('vendors.create');
    Route::post('/store',[VendorController::class,'store'])->name('vendors.store');
    Route::get('/show/{id}',[VendorController::class,'show'])->name('vendors.show');
    Route::get('/edit/{id}',[VendorController::class,'edit'])->name('vendors.edit');
    Route::post('/update{id}',[VendorController::class,'update'])->name('vendors.update');
    Route::get('/delete{id}',[VendorController::class,'delete'])->name('vendors.delete');
    Route::get('/trashlist',[VendorController::class,'trashlist'])->name('vendors.trashlist');
    Route::get('/restore/{id}',[VendorController::class,'restore'])->name('vendors.restore');
    Route::get('/permanentdelete/{id}',[VendorController::class,'permanentDelete'])->name('vendors.permanentdelete');

});


// Route::prefix('orders')->group(function(){
//     Route::get('/',[OrderController::class,'index'])->name('orders.index');
//     Route::get('/create',[OrderController::class,'create'])->name('orders.create');
//     Route::post('/store',[OrderController::class,'store'])->name('orders.store');
//     Route::get('/show/{id}',[OrderController::class,'show'])->name('orders.show');
//     Route::get('/edit/{id}',[OrderController::class,'edit'])->name('orders.edit');
//     Route::get('/update{id}',[OrderController::class,'update'])->name('orders.update');
//     Route::get('/delete{id}',[OrderController::class,'delete'])->name('orders.delete');
//     Route::get('/trashlist',[OrderController::class,'trashlist'])->name('orders.trashlist');
//     Route::get('/restore/{id}',[OrderController::class,'restore'])->name('orders.restore');
//     Route::get('/permanentdelete/{id}',[OrderController::class,'permanentDelete'])->name('orders.permanentdelete');
// //store
// });

Route::prefix('stores')->group(function(){
    Route::get('/',[StoreController::class,'index'])->name('store.index');
    Route::get('/create',[StoreController::class,'create'])->name('store.create');
    Route::post('/store',[StoreController::class,'store'])->name('store.store');
    Route::get('/show/{id}',[StoreController::class,'show'])->name('store.show');
    Route::get('/edit/{id}',[StoreController::class,'edit'])->name('store.edit');
    Route::post('/update/{id}',[StoreController::class,'update'])->name('store.update');
    Route::get('/delete/{id}',[StoreController::class,'destroy'])->name('store.delete');
    Route::get('/trashlist',[StoreController::class,'trashlist'])->name('store.trashlist');
    Route::get('/restore/{id}',[StoreController::class,'restore'])->name('store.restore');
    Route::get('/permanentdelete/{id}',[StoreController::class,'permanentDelete'])->name('store.permanentdelete');
    

});


//roles

Route::prefix('roles')->group(function(){
    Route::get('/',[RoleController::class,'index'])->name('roles.index');
    Route::get('/create',[RoleController::class,'create'])->name('roles.create');
    Route::post('/store',[RoleController::class,'store'])->name('roles.store');
    Route::get('/show/{id}',[RoleController::class,'show'])->name('roles.show');
    Route::get('/edit/{id}',[RoleController::class,'edit'])->name('roles.edit');
    Route::post('/update{id}',[RoleController::class,'update'])->name('roles.update');
    Route::get('/delete{id}',[RoleController::class,'destroy'])->name('roles.delete');
    Route::get('/trashlist',[RoleController::class,'trashlist'])->name('roles.trashlist');
    Route::get('/restore/{id}',[RoleController::class,'restore'])->name('roles.restore');
    Route::get('/permanentdelete/{id}',[RoleController::class,'permanentDelete'])->name('roles.permanentdelete');

});

Route::prefix('boxes')->group(function(){
    Route::get('/',[BoxController::class,'index'])->name('boxes.index');
    Route::get('/create',[BoxController::class,'create'])->name('boxes.create');
    Route::post('/store',[BoxController::class,'store'])->name('boxes.store');
    Route::get('/show/{id}',[BoxController::class,'show'])->name('boxes.show');
    Route::get('/edit/{id}',[BoxController::class,'edit'])->name('boxes.edit');
    Route::post('/update{id}',[BoxController::class,'update'])->name('boxes.update');
    Route::get('/delete{id}',[BoxController::class,'destroy'])->name('boxes.delete');
    Route::get('/trashlist',[BoxController::class,'trashlist'])->name('boxes.trashlist');
    Route::get('/restore/{id}',[BoxController::class,'restore'])->name('boxes.restore');
    Route::get('/permanentdelete/{id}',[BoxController::class,'permanentDelete'])->name('boxes.permanentdelete');

});

Route::prefix('orders')->group(function(){
    Route::get('/',[OrderController::class,'index'])->name('orders.index');
    Route::get('/create',[OrderController::class,'create'])->name('orders.create');
    Route::post('/store',[OrderController::class,'store'])->name('orders.store');
    Route::get('/show/{id}',[OrderController::class,'show'])->name('orders.show');
    Route::get('/edit/{id}',[OrderController::class,'edit'])->name('orders.edit');
    Route::post('/update{id}',[OrderController::class,'update'])->name('orders.update');
    Route::get('/delete{id}',[OrderController::class,'delete'])->name('orders.delete');
    Route::get('/trashlist',[OrderController::class,'trashlist'])->name('orders.trashlist');
    Route::get('/restore/{id}',[OrderController::class,'restore'])->name('orders.restore');
    Route::get('/permanentdelete/{id}',[OrderController::class,'permanentDelete'])->name('orders.permanentdelete');
});



Route::prefix('frontend')->group(function(){
    Route::get('/',FrontendController::class,'index')->name('frontend.index');
});


Route::get('/user/profile/{id}', [ProfileController::class, 'profileEdit'])->name('users.profile');
Route::post('/user/profile_update/{id}', [ProfileController::class, 'profileUpdate'])->name('users.profile_update');

Route::get('/user/change_password', [ProfileController::class, 'changePassword'])->name('users.change_password');
Route::post('/user/update_password', [ProfileController::class, 'updatePassword'])->name('users.update_password');
