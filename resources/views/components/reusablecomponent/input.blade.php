@props(['name', 'type'=>'text'])

<div>
    <label for="{{ $name }}">{{ ucwords($name) }}</label>
</div>
<div>
<input type="{{ $type }}" id="{{ $name }}" name="{{ $name }}" value="{{ old($name) }}" mb-4 class="form-control">
</div>
@error($name)
<span class="text-danger">{{ $message }}</span>
@enderror