<button type="submit" class="btn btn-sm btn-primary float-right mt-2">
    {{ $slot }}
</button>
