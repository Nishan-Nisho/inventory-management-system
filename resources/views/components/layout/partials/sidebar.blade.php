<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{route('dashboard')}}">
            <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-laugh-wink"></i>
            </div>
            <div class="fs-1">BMS</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="{{route('dashboard')}}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseOnee"
                aria-expanded="true" aria-controls="collapseOnee">
                <i class="fas fa-fw fa-cog"></i>
                {{-- <i class="fa-solid fa-align-left"></i> --}}
                <span>Vendor</span>
            </a>
            <div id="collapseOnee" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('vendors.index') }}">View Vendors</a>
                    <a class="collapse-item" href="{{ route('vendors.create') }}">Create Vendor</a>
                </div>
            </div>
        </li>

        <hr class="sidebar-divider">
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="fas fa-fw fa-cog"></i>
                {{-- <i class="fa-solid fa-align-left"></i> --}}
                <span>Customer</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('customer.index') }}">View Customers</a>
                    <a class="collapse-item" href="{{ route('customer.create') }}">Add Customers</a>
                </div>
            </div>
        </li>

        

        <!-- Divider -->
        <hr class="sidebar-divider">


        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages"
                aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-fw fa-folder"></i>
                <span>Products</span>
            </a>
            <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Products:</h6>
                    <a class="collapse-item" href="{{ route('products.index') }}">Product List</a>
                  
                </div>
            </div>
        </li>

        <!-- Nav Item - Pages Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagess"
                aria-expanded="true" aria-controls="collapsePagess">
                <i class="fas fa-fw fa-folder"></i>
                <span>Orders</span>
            </a>
            <div id="collapsePagess" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Products:</h6>
                    <a class="collapse-item" href="{{ route('orders.index') }}">Order List</a>
                    <a class="collapse-item" href="{{ route('orders.create') }}">Create Order</a>
                    
                </div>
            </div>
        </li>


        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePagesss"
                aria-expanded="true" aria-controls="collapsePages">
                <i class="fas fa-fw fa-folder"></i>
                <span>Category</span>
            </a>
            <div id="collapsePagesss" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Category:</h6>
                    <a class="collapse-item" href="{{ route('categories.create') }}">Add Category</a>
                    <a class="collapse-item" href="{{ route('categories.index') }}">Category List</a>
                  
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <hr class="sidebar-divider">
            <!-- Nav Item - Utilities Collapse Menu -->
            <!-- Nav Item - Customers Collapse Menu -->
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUtilities"
                aria-expanded="true" aria-controls="collapseUtilities">
                <i class="fas fa-fw fa-wrench"></i>
                <span>Inventory</span>
            </a>
            <div id="collapseUtilities" class="collapse" aria-labelledby="headingUtilities"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Manage:</h6>
                    <a class="collapse-item" href="{{ route('inventory') }}">View</a>
                    <a class="collapse-item" href="{{ route('store.index') }}">Stores</a>
                    <a class="collapse-item" href="{{ route('boxes.index') }}">Box</a>
                </div>
            </div>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

            

</ul>
<!-- End of Sidebar -->
