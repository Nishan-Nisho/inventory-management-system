<x-layout.master>

  <div class="container" style="padding-top: 20px;">
    <h1 class="page-header">Edit Profile</h1>
    <div class="row">
      <!-- left column -->
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="text-center">
        

          <div class="card-body">


            @if(file_exists(storage_path().'/app/public/users/'.$user->profile->image ) && (!is_null($user->profile->image)))

            <img src="{{ asset('storage/users/'.$user->profile->image) }}" height="100">

            @else
               <img src="{{ asset('img/default.png') }}" />
            @endif

          </div>

        </div>
      </div>
      <!-- edit form column -->
      <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
        <div class="alert alert-info alert-dismissable">
          <a class="panel-close close" data-dismiss="alert">×</a> 
          <i class="fa fa-coffee"></i>
          This is an <strong>.alert</strong>. Use this to show important messages to the user.
        </div>
        <h3>Personal info</h3>
        <form action="{{ route('users.profile_update', $user->id) }}" method="POST" class="form-horizontal" role="form">
          @csrf
          <div class="form-group">
            <label class="col-lg-3 control-label">Full Name:</label>
            <div class="col-lg-8">
              <input class="form-control" name="name" value="{{ $user->name ?? '' }}" type="text-muted">
            </div>
          </div>       
          <div class="form-group">
            <label class="col-lg-3 control-label">Email:</label>
            <div class="col-lg-8">
              <input class="form-control" name="email" value="{{ $user->email ?? '' }}" type="text-muted" disabled>
            </div>
          </div>


          <div class="form-group">
            <label class="col-lg-3 control-label">Image:</label>
            <div class="col-lg-8">
              <input class="form-control" name="image" value="{{ $user->profile->image ?? '' }}" type="file">
            </div>
          </div> 

          <div class="form-group">
              <label class="col-lg-3 control-label">Address:</label>
              <div class="col-lg-8">
                <input class="form-control" name="address" value="{{ $user->profile->address ?? '' }}" type="text">
              </div>
            </div> 

          <div class="form-group">
              <label class="col-lg-3 control-label">Phone Number:</label>
              <div class="col-lg-8">
                <input class="form-control" name="phone_number" value="{{ $user->profile->phone_number ?? '' }}" type="text">
              </div>
            </div>  

           <div class="form-group">
              <label class="col-lg-3 control-label">Country:</label>
              <div class="col-lg-8">
                <input class="form-control" name="country" value="{{ $user->profile->country ?? '' }}" type="text">
              </div>
            </div> 
            
          {{-- <div class="form-group">
            <label class="col-md-3 control-label">Password:</label>
            <div class="col-md-8">
              <input class="form-control" value="" type="password">
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Confirm password:</label>
            <div class="col-md-8">
              <input class="form-control" value="11111122333" type="password">
            </div>
          </div> --}}
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" value="Save Changes" type="submit">
              <span></span>
              <input class="btn btn-default" value="Cancel" type="reset">
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-layout.master>