<x-layout.master>

  <div class="container" style="padding-top: 20px;">
    <h1 class="page-header">Change Password</h1>
    {{-- @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif --}}
    <div class="row">
      <!-- left column -->
      <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="text-center">
        </div>
      </div>
      <!-- edit form column -->
      <div class="col-md-8 col-sm-6 col-xs-12 personal-info">
        <form action="{{ route('users.update_password') }}" method="post" class="form-horizontal" role="form">
          @csrf     
          <div class="form-group">
            <label class="col-md-3 control-label">Old Password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" name="oldpassword">
              @error('oldpassword')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">New password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" name="newpassword">
              @error('newpassword')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>          
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label">Confirm password:</label>
            <div class="col-md-8">
              <input class="form-control" type="password" name="confirmpassword">
              @error('confirmpassword')
              <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
          </div>
          <div class="form-group">
            <label class="col-md-3 control-label"></label>
            <div class="col-md-8">
              <input class="btn btn-primary" value="Update Password" type="submit">
              <span></span>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-layout.master>