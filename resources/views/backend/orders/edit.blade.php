<x-layout.master>
    <x-slot name="pageTitle">Orders Edit</x-slot>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('orders.index') }} " class="btn btn-sm btn-primary float-end">Orders List</a>
                </div>
    
                <div class="card-body">
                    <form action="{{ route('orders.update',$order->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        
                        <div>
                            <div>
                                <label for="">Customer Name</label>
                            </div>
                            <input type="text" name="name" value="{{ $order->customer->name }}" class="form-control">
                        </div>

                        <div>
                            <div>
                                <label for="">Order Date</label>
                            </div>
                            <input type="toDateTimeLocalString" name="order_date" value="{{ $order->order_date }}" class="form-control">
                        </div>

                        {{-- <div>
                            <div>
                                <label for="">Order Status</label>
                            </div>
                            <input type="text" name="order_status" value="{{ $order->order_status }}" class="form-control">
                        </div> --}}
    
                        {{-- Reusable Button Component Used here --}}
                        <x-reusablecomponent.button>Save</x-reusablecomponent.button>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</x-layout.master>