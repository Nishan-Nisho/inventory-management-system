<x-layout.master>

    <x-slot name="pageTitle">Orders Show</x-slot>
    <div class="container">
        <div class="row">
            <div class="col-12">
             <a href="{{ route('orders.index') }}" class="btn btn-sm btn-info mb-2 float-end">Orders List</a>
             
             
             
             
             
     
             <table class="table table-striped table-bordered">
                 <tr class="text-center">
                    <th class="text-center">Name</th>
                    <th class="text-center">Order Date</th>
                    <th class="text-center">Status</th>
                 </tr>
         
                 <tr class="text-center">
                     <td class="text-center">{{ $order->customer->name ?? ''}}</td>
                     <td class="text-center">{{ $order->order_date ?? ''}}</td>
                     <td class="text-center">{{ $order->order_status ?? ''}}</td>
                 </tr>
             </table>
            </div>
        </div>
    </div>
 
     {{-- <p>Name: {{ $color->colorName ?? '' }}</p>
     <p>Photo: {{ $color->colorImage ?? '' }}</p> --}}
 
</x-layout.master>