<x-layout.master>
    <div class="container">
        <div class="row">
            <div class="col-12">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('orders.create') }}" class="btn btn-sm btn-primary float-start">Make an Order</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>
                    --}}
                    <a href="{{ route('orders.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash List</a>
                    
                    
                </div>
        
                @if (session('message'))
                    <p class="alert alert-primary">{{ session('message') }}</p>
                @endif 
        
                {{-- <div class="card-body">
                    
                        
                            <table class="table table-hover  table-bordered">
                                <tbody>
                                    <tr>
                                        
                                        <th class="text-center">Customer Name</th>
                                        
                                        @foreach ($orders as $order)
                                            <td class="text-center">
                                                <select class="form-select" name="name" aria-label="Default select example">
                                                    <option value="">{{ $order->customer->name }}</option>

                                                    @foreach ($customers as $customer)
                                                    <option value="">{{ $customer->name }}</option>
                                                    @endforeach
                                                  </select> 
                                            </td> 
                                        @endforeach
                                    </tr>
                                    <tr>
                                        <th class="text-center">Order Date</th>
                                        <td class="text-center">{{ $order->order_date }}</td>
                                    </tr>
                                    
                                        
                                        <tr>
                                            <th class="text-center">Order Status</th>
                                            <td class="text-center">{{ $order->order_status }}</td>
                                        </tr>
                                        
                                        <tr>
                                            <th class="text-center">Action</th>
                                             
                                        <td class="text-center">
                                            <a href="{{ route('orders.show',$order->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                            <a href="{{ route('orders.edit',$order->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                            <a href="{{ route('orders.delete',$order->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                        </tr>
                                        
                                    </tr>
                                </tbody>
                               
                            </table>
                        </div>
                    </div>
                </div> --}}
                <div class="card-body">
                    
                        
                    <table class="table table-hover table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="text-center">ID#</th>
                                <th class="text-center">Customer Name</th>
                                <th class="text-center">Product Name</th>
                                <th class="text-center">Product Quantity</th>
                                <th class="text-center">Unit Price</th>
                                <th class="text-center">Total Price</th>
                                <th class="text-center">Order Date</th>
                                {{-- <th class="text-center">Order Status</th> --}}
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        @php
                            $i=1;
                        @endphp
                        <tbody>
                            @foreach ($orders as $order)
                            <tr>
                                <td class="text-center">{{ $i++ }}</td>
                                <td class="text-center">
                                
                                        {{$order->customer->name }}
                                
                                </td>

                                <td class="text-center">
                                
                                    {{$order->product->name }}
                            
                            </td>

                            <td class="text-center">{{ $order->quantity }}</td>
                            <td class="text-center">{{ $order->unit_price }}</td>
                            <td class="text-center">{{ $order->total_price }}</td>
                                <td class="text-center">{{ $order->order_date }}</td>
                                {{-- <td class="text-center">{{ $order->order_status }}</td> --}}
            
                                
                                <td class="text-center">
                                    <a href="{{ route('orders.show',$order->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                    <a href="{{ route('orders.edit',$order->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                    <a href="{{ route('orders.delete',$order->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                </td> 
                            </tr>
                            @endforeach
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            </div>
    </div>
    
</x-layout.master>