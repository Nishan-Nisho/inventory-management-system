<x-layout.master>
    <div class="row">
        <div class="col-10 offset-1">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('orders.index') }}" class="btn btn-sm btn-primary float-right">Orders List</a>                
            </div>
    
            @if (session('message'))
                <p class="alert alert-danger text-danger">{{ session('message') }}</p>
            @endif 
    
            <div class="card-body">
                
                    <p class="btn btn-danger form-control">Trashed List</p>
                        <table class="table table-hover  table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID#</th>
                                    <th class="text-center">Customer Name</th>
                                    <th class="text-center">Order Date</th>
                                    <th class="text-center">Order Status</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                           
                            <tbody>
                                @foreach ($order as $key=>$order)
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td class="text-center">{{ $order->customer->name }}</td>
                                    
                                    <td class="text-center">
                                        <a href="{{ route('orders.restore',$order->id) }}" class="btn btn-sm btn-info">Restore</a>
                                        <a href="{{ route('orders.permanentdelete',$order->id) }}" class="btn btn-sm btn-warning">Permanent Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </x-layout.master>