<x-layout.master>
    <x-slot name="pageTitle">Orders Create</x-slot>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
        <div class="col-10 offset-1">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('orders.index') }}" class="btn btn-sm btn-primary float-end">Orders List</a>
                </div>

                <div class="card-body">
                    <form action="{{ route('orders.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div>
                            
                            <div>
                                <label for="">Customer Name</label>
                                <select name="customer_id" id="" class="form-control">
                                    <option value="">Select Customer</option>
                                    @foreach ($customers as $customer)
                                        <option value="{{ $customer->id }}">{{ $customer->name ?? '' }}</option>
                                    @endforeach
                                </select>
                            </div>


                            
                            <div>
                                <label for="">Product Name</label>
                                <select name="product_id" id="" class="form-control">
                                    <option value="">Select Product</option>
                                    @foreach ($products as $product)
                                        <option value="{{ $product->id }}">{{ $product->name ?? '' }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div>
                                <label for="">Unit Price</label>
                                <input type="number" name="unit_price" class="form-control">
                            </div>

                            <div>
                                <label for="">Quantity</label>
                                <input type="text" name="quantity" class="form-control">
                            </div>

                            <div>
                                <label for="">Total Price</label>
                                <input type="text" name="total_price" class="form-control">
                            </div>

                            <div>
                                <label for="">Order Date</label>
                                <input type="date" name="order_date" class="form-control">
                            </div>
                            
                            {{-- <div>
                                <label for="">Order Status</label>
                                <select class="custom-select" name="order_status" id="">
                                    <option value="">Select Order Status</option>
                                    <option value="Approved">Approved</option>
                                    <option value="Pending">Pending</option>
                                    <option value="Cancel">Cancel</option>
                                    <option value="Delivered">Delivered</option>
                                </select>
                            </div> --}}

                            <x-reusablecomponent.button>Save</x-reusablecomponent.button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-layout.master>