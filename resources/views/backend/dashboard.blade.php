<x-layout.master>
<div class="container" >
 <div class="row text-center m-2" >
  
   <div class="col-12 my-3 p-0 m-0" >
           <h1>Pondit Sales Ltd.</h1>
   </div>
</div>

<div class="row mb-4">

    <div class="col-4">
      <div class="card shadow p-3 bg-body rounded" style="width: 16rem; height: 8rem">
        <a style="text-decoration:none" href="{{ route('customer.index') }}">
        <div class="card-body d-flex">
          <div><i class="fa fa-users p-2" aria-hidden="true"></i></div>
          <div><h5 class="card-title p-1">Customers</h5></div>
         </div>
         </a>
      </div>
    </div>

    <div class="col-4">
      <div class="card shadow p-3 bg-body rounded" style="width: 18rem; height: 8rem">
        <a style="text-decoration:none" href="{{ route('orders.index') }}">
        <div class="card-body d-flex">
          <div><i class="fa fa-shopping-cart p-2" aria-hidden="true"></i></div>
          <div><h5 class="card-title p-1">Orders</h5></div>
         </div>
         </a>
      </div>
    </div>

    <div class="col-4">
      <div class="card shadow p-3 bg-body rounded" style="width: 18rem; height: 8rem">
        <a style="text-decoration:none" href="{{ route('products.index') }}">
        <div class="card-body d-flex">
          <div><i class="fa fa-cubes p-2" aria-hidden="true"></i></div>
          <div><h5 class="card-title p-1">Products</h5></div>
         </div>
         </a>
      </div>
    </div>
 </div>

 <div class="row">

 <!-- Area Chart -->
  <div class="col-12" >
      <div class="card shadow mb-4">
          <!-- Card Header - Dropdown -->
          <div
              class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
              <h6 class="m-0 font-weight-bold text-primary">Sales Overview</h6>
          </div>
          <!-- Card Body -->
          <div class="card-body">
              <div class="chart-area">
                  <canvas id="myAreaChart"></canvas>
              </div>
          </div>
      </div>
    </div>
  </div>



</div>
</x-layout.master>
