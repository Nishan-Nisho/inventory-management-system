<x-layout.master>
    <x-slot name="pageTitle">Vendors Create</x-slot>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
        <div class="col-12">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('vendors.index') }}" class="btn btn-sm btn-primary float-end">Vendors List</a>
                </div>

                <div class="row">
                    <div class="col-6 offset-3">
                        <div class="card-body">
                            <form action="{{ route('vendors.store') }}" method="POST" enctype="multipart/form-data">
                                @csrf
        
                                <div>
                                    
                                    {{-- Reusable Input Component used here --}}
                                    <x-reusablecomponent.input name="name"/>
                                    <x-reusablecomponent.input name="email" type="email"/>
                                    <x-reusablecomponent.input name="phone"/>
                                    <x-reusablecomponent.input name="address"/>
                                    <x-reusablecomponent.input name="image" type="file"/>
                                    <x-reusablecomponent.input name="remarks"/>
                                    
                                    {{-- <div>
                                        <label for="">Remarks</label>
                                    <select name="remarks" id="" class="form-control">
                                        <option value="">Seleted Remarks</option>
                                        <option value="{{ $vendors->id }}">Satisfactory</option>
                                        <option value="">Disappointed</option>
                                        <option value="">Blacklisted</option>
                                    </select>
                                    </div> --}}
        
                                    <x-reusablecomponent.button>Save</x-reusablecomponent.button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-layout.master>