<x-layout.master>
    <x-slot name="pageTitle">Vendors Edit</x-slot>
<div class="row">
    <div class="col-8 offset-2">
        <div class="card bg-light">
            <div class="card-header">
                <a href="{{ route('vendors.index') }} " class="btn btn-sm btn-primary float-end">Vendors List</a>
            </div>

            <div class="card-body">
                <form action="{{ route('vendors.update',$vendor->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div>
                        <div>
                            <label for="">Name</label>
                        </div>
                        <input type="text" name="name" value="{{ $vendor->name }}" class="form-control">
                    </div>

                    <div>
                        <div>
                            <label for="">Email</label>
                        </div>
                        <input type="email" name="email" value="{{ $vendor->email }}" class="form-control">
                    </div>

                    <div>
                        <div>
                            <label for="">Phone</label>
                        </div>
                        <input type="text" name="phone" value="{{ $vendor->phone }}" class="form-control">
                    </div>

                    <div>
                        <div>
                            <label for="">Address</label>
                        </div>
                        <input type="text" name="address" value="{{ $vendor->address }}" class="form-control">
                    </div>

                    <div>
                    <div>
                        <label for="">photo</label>
                    </div>
                    <input type="file" name="image" value="{{ $vendor->image }}" class="form-control">
                    <img src="{{ asset('storage/vendors/'.$vendor->image) }}" height="100" alt="">
                    </div>

                    <div>
                        <div>
                            <label for="">Remarks</label>
                        </div>
                        <input type="text" name="remarks" value="{{ $vendor->remarks }}" class="form-control">
                        {{-- <div>
                            <label>Remarks</label>
                            <div>
                            <select name="remarks" class="form-control">
                                
                                @foreach ($vendor as $ven)
                                
                                    <option value="">{{ $ven->remarks }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div> --}}
                        
                    </div>

                    
                    <x-reusablecomponent.button>Save</x-reusablecomponent.button>

                </form>
            </div>
        </div>
    </div>
</x-layout.master>