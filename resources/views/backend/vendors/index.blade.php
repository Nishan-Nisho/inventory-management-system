<x-layout.master>
<div class="container">
    <div class="row">
        <div class="col-12">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('vendors.create') }}" class="btn btn-sm btn-primary float-start">Add Vendor</a>
                {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>--}}
                <a href="{{ route('vendors.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash List</a>
                
            </div>
    
            @if (session('message'))
                <p class="text-primary">{{ session('message') }}</p>
            @endif 
    
            <div class="card-body">
                
                    
                        <table class="table table-hover table-bordered table-responsive">
                            <thead>
                                <tr>
                                    <th class="text-center">ID#</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">Email</th>
                                    <th class="text-center">Phone</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Photo</th>
                                    <th class="text-center">Remarks</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                @foreach ($vendors as $key=>$vendor)
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td class="text-center">{{ $vendor->name }}</td>
                                    <td class="text-center">{{ $vendor->email }}</td>
                                    <td class="text-center">{{ $vendor->phone }}</td>
                                    <td class="text-center">{{ $vendor->address }}</td>
                                    {{-- <td class="text-center">{{ $vendor->image }}</td> --}}
                                    <td class="text-center">
                                         @if (file_exists(storage_path().'/app/public/vendors/'.$vendor->image) && (!is_null($vendor->image)))
     
                                            <img src="{{ asset('storage/vendors/'.$vendor->image) }}" height="120" alt="">
     
                                        @else
                                        <p class="text-danger">Img not Found</p>
                                        @endif
                                    </td>
                                    <td class="text-center">{{ $vendor->remarks }}</td>
                                    
                                    <td class="text-center d-flex">
                                        <a href="{{ route('vendors.show',$vendor->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                        <a href="{{ route('vendors.edit',$vendor->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                        <a href="{{ route('vendors.delete',$vendor->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        
                                    </td> 
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    

</div>
</x-layout.master>