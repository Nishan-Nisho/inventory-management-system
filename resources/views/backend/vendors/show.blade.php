<x-layout.master>

    <x-slot name="pageTitle">Vendors Show</x-slot>
    <div class="container">
        <div class="row">
            <div class="col-12">
             <a href="{{ route('vendors.index') }}" class="btn btn-sm btn-info mb-2 float-end">Back to Vendors List</a>
             
             
             
             
             
     
             <table class="table table-striped table-bordered">
                 <tr class="text-center">
                    <th class="text-center">Name</th>
                    <th class="text-center">Email</th>
                    <th class="text-center">Phone</th>
                    <th class="text-center">Address</th>
                    <th class="text-center">Photo</th>
                    <th class="text-center">Remarks</th>
                 </tr>
         
                 <tr class="text-center">
                     <td class="text-center">{{ $vendor->name ?? ''}}</td>
                     <td class="text-center">{{ $vendor->email ?? ''}}</td>
                     <td class="text-center">{{ $vendor->phone ?? ''}}</td>
                     <td class="text-center">{{ $vendor->address ?? ''}}</td>
                     <td class="text-center">
                         {{-- Image Upload code goes here --}}
                         @if (file_exists(storage_path().'/app/public/vendors/'.$vendor->image) && (!is_null($vendor->image)))
     
                         <img src="{{ asset('storage/vendors/'.$vendor->image) }}" height="100" alt="">
                         
                         @else
                         <p class="text-danger">Img not Found</p>
                         @endif
                         {{-- Image upload code end --}}
                     </td>
                     <td class="text-center">{{ $vendor->remarks ?? ''}}</td>
                     {{-- <td class="text-center">{{ $color->colorImage ?? ''}}</td> --}}
                 </tr>
             </table>
            </div>
        </div>
    </div>
 
     {{-- <p>Name: {{ $color->colorName ?? '' }}</p>
     <p>Photo: {{ $color->colorImage ?? '' }}</p> --}}
 
</x-layout.master>