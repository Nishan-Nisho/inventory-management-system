<x-layout.master>

    <div class="container-fluid">
        <div class="row">
            <div class="col-11">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('products.create') }}" class="btn btn-sm btn-primary float-start">Add Products</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>--}}
                    <a href="" class="btn btn-sm btn-warning me-2 float-end">Trash List</a>
                    
                </div>
        
                @if (session('message'))
                    <p class="text-primary">{{ session('message') }}</p>
                @endif 
        
                <div class="card-body">                    
                            <table class="table table-hover table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID#</th>
                                        <th class="text-center">Category Name</th>
                                        <th class="text-center">Vendor Name</th>
                                        <th class="text-center">product Name</th>
                                        <th class="text-center">Description</th>
                                        <th class="text-center">Image</th>
                                        <th class="text-center">Storage</th>
                                        <th class="text-center">Boxes</th>
                                        <th class="text-center">Unit Price</th>
                                        <th class="text-center">Quantity Sold</th>
                                        <th class="text-center">Purchase Date</th>
                                        <th class="text-center">Total Price</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                              
                                <tbody>
                                    @foreach ($products as $key=>$product)
                                    <tr>
                                        <td class="text-center">{{ $key+1 }}</td>
                                        
                                        <td class="text-center">
                                            
                                                {{ $product->category->category_name }}
                                                                              
                                        </td>

                                        <td class="text-center">
                                        
                                           @isset($product->vendors)
                                               @foreach ($product->vendors as $vendor)
                                                   <span class="badge badge-sm badge-primary">
                                                       {{ $vendor->name ?? '' }}
                                                   </span>
                                               @endforeach
                                           @endisset
                                                                                  
                                        </td>

                                        
                                        <td class="text-center">{{ $product->name }}</td>
                                        <td class="text-center">{{ $product->description }}</td>
                                        <td class="text-center">
                                            @if (file_exists(storage_path().'/app/public/products/'.$product->image) && (!is_null($product->image)))
                                 
                                                     <img src="{{ asset('storage/products/'.$product->image) }}" height="100" alt="">
                                                     
                                                     @else
                                                     <p class="text-danger">Img not Found</p>
                                                     @endif
                                        </td>
                                       

                                        <td class="text-center">
                                            {{ $product->store->name }}
                                        </td>

                                        <td class="text-center">
                                            {{ $product->box->name }}
                                        </td>

                                        

                                        <td class="text-center">{{ $product->unit_price }}</td>
                                        <td class="text-center">{{ $product->quantity_sold }}</td>
                                        <td class="text-center">{{ $product->buy_date }}</td>
                                        <td class="text-center">{{ $product->total_price }}</td>
                                   
                                                           
                                        <td class="text-center d-flex m-0">
                                            <a href="{{ route('products.show',$product->id) }}" class="btn btn-sm btn-info m-1">Show</a>
                                            <a href="{{ route('products.edit',$product->id) }}" class="btn btn-sm btn-warning m-1">Edit</a>
                                            <a href="{{ route('products.delete',$product->id) }}" class="btn btn-sm btn-danger m-1">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        
    
    </div>
</x-layout.master>