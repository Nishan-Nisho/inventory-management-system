<x-layout.master>
    <x-slot name=pageTitle>show Customer</x-slot>

<div class="row">
    <div class="col-8 offset-2">
     <a href="{{ route('products.index') }}" class="btn btn-sm btn-info mb-2 float-end">Back to Products List</a>
     
     <table class="table table-striped table-bordered">
         <tr class="text-center">
            <th class="text-center">Id</th>
            <th class="text-center">Category</th>
            <th class="text-center">Vendor</th>
            <th class="text-center">Description</th>
            <th class="text-center">Image</th>
            <th class="text-center">Unit</th>
            <th class="text-center">Quantity</th>
            <th class="text-center">Status</th>
            <th class="text-center">Purchase Date</th>
            <th class="text-center">Selling Price</th>
            <th class="text-center">Remarks</th>
         </tr>
 
         <tr class="text-center">
            <td class="text-center">{{ $product->category_id}}</td>
            <td class="text-center">{{ $product->vendor->vendor_id }}</td>
            <td class="text-center">{{ $product->name }}</td>
            <td class="text-center">{{ $product->description }}</td>
            <td class="text-center">
                @if (file_exists(storage_path().'/app/public/products/'.$product->image) && (!is_null($product->image)))
     
                         <img src="{{ asset('storage/products/'.$product->image) }}" height="100" alt="">
                         
                         @else
                         <p class="text-danger">Img not Found</p>
                         @endif
            </td>
            <td class="text-center">{{ $product->unit_price }}</td>
            <td class="text-center">{{ $product->quantity_sold }}</td>
            <td class="text-center">{{ $product->status }}</td>
            <td class="text-center">{{ $product->buy_date }}</td>
            <td class="text-center">{{ $product->selling_price }}</td>
            <td class="text-center">{{ $product->remarks }}</td>
             
         </tr>
     </table>
    </div>
</div>
</x-layout.master>