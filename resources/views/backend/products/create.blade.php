<x-layout.master>
    <x-slot name="pageTitle">Products Create</x-slot>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

<div class="container">
    <div class="row">
        <div class="col-12">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="" class="btn btn-sm btn-primary float-end">Products List</a>
                </div>

               <div class="row">
                   <div class="col-6 offset-3">
                    <div class="card-body">
                    
                        <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
    
                            <div>
                                {{-- Reusable Input Component used here --}}
                                
                                <label>Categories</label>
                                <select name="category_id" class="form-control">
                                    <option value="">Select Category</option>
                                    @foreach ($categories as $cat)
                                    <option value="{{ $cat->id }}"> {{ $cat->category_name ?? '' }}</option>
                                    @endforeach
                                </select>

                                <label>Vendors</label>
                                <select name="vendor_id" class="form-control">
                                    <option value="">Select Vendor</option>
                                    @foreach ($vendors as $vendor)
                                    <option value="{{ $vendor->id }}"> {{ $vendor->name ?? '' }}</option>
                                    @endforeach
                                </select>
                                <div>
                                    <label for="">Product Name</label>
                                    <div>
                                        <input type="text" name="name" value="" class="form-control">
                                    </div>
                                </div>
                                <x-reusablecomponent.input name="description" />
                                <x-reusablecomponent.input name="image" type="file"/>
                                <div>
                                    <label for="">Store Name</label>
                                    <select name="store_id" id="" class="form-control">
                                        <option value="">Select Store</option>
                                        @foreach ($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name ?? '' }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <label for="">Box Name</label>
                                    <select name="box_id" id="" class="form-control">
                                        <option value="">Select Box</option>
                                        @foreach ($boxes as $box)
                                            <option value="{{ $box->id }}">{{ $box->name ?? '' }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <label for="">Buy Date</label>
                                    <input type="date" name="buy_date" class="form-control">
                                </div>
                                
                                <div>
                                    <label for="">Unit Price</label>
                                    <input type="number" name="unit_price" class="form-control">
                                </div>
                                
                                
                                <div>
                                    <label for="">Total Price</label>
                                    <input type="number" name="total_price" class="form-control" value="20">
                                </div>
                                

                                 <div>
                                     <label for="">Quantity Sold</label>
                                     <input type="number" name="quantity_sold" class="form-control">
                                 </div>
                                
                                 {{-- <div>
                                     
                                <label for="">Product Status</label>
                                <select name="status" id="" class="form-control">
                                    <option value="">Select Status</option>
                                    <option value="">Sold Out</option>
                                    <option value="">Stocked</option>
                                    <option value="">Empty</option>
                                </select>
                                 </div> --}}
                                

    
                                <x-reusablecomponent.button>Save</x-reusablecomponent.button>
                        </form>
                    </div>
                   </div>
               </div>
            </div>
        </div>
    </div>
</div>

</x-layout.master>