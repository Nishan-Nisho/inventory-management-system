</x-layout.master>
    <x-slot name="pageTitle">Product Edit</x-slot>
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('products.index') }} " class="btn btn-sm btn-primary float-end">product List</a>
                </div>
    
                <div class="card-body">
                            {{-- @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                            @endif --}}
                             <form class="form-horizontal" action="{{ route('products.update',$product->id) }}" method="POST">
                                 @csrf
                                 <div>
                                    <div>
                                        <label class="col-md-4 control-label">Category</label>
                                    </div>
                                    <input type="text" name="category_id" value="{{ $product->category_id }}" class="form-control">
                                </div>

                                 <div>
                                   <div>
                                    <label class="col-md-4 control-label">Vendor</label>
                                   </div>
                                    <input type="text" name="email"  value="{{ $product->vendor_id}}" class="form-control" />
                                </div>
                                 
                                <div>
                                    <div>
                                     <label class="col-md-4 control-label">Name</label>
                                    </div>
                                     <input type="text" name="name"  value="{{ $product->name}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Image</label>
                                    </div>
                                     <input type="text" name="image"  value="{{ $product->image}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Unit Price</label>
                                    </div>
                                     <input type="text" name="unit_price"  value="{{ $product->unit_price}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Quantity Sold</label>
                                    </div>
                                     <input type="text" name="quantity_sold"  value="{{ $product->quantity_sold}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Status</label>
                                    </div>
                                     <input type="text" name="status"  value="{{ $product->status}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Buying date</label>
                                    </div>
                                     <input type="date" name="buy_date"  value="{{ $product->buy_date}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Selling Price</label>
                                    </div>
                                     <input type="date" name="selling_price"  value="{{ $product->selling_price}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Remarks</label>
                                    </div>
                                     <input type="date" name="remarks"  value="{{ $product->remarks}}" class="form-control" />
                                 </div>
                                 
                                <x-reusablecomponent.button>Update</x-reusablecomponent.button>
                             </form>
                            </div>
                        </div>
                    </div>
                </div>
</x-layout.master>