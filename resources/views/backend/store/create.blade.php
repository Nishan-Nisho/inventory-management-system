<x-layout.master>
    <x-slot name="pageTitle">store Create</x-slot>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
        <div class="col-10 offset-1">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('store.index') }}" class="btn btn-sm btn-primary float-end">store List</a>
                </div>

                <div class="card-body">
                    <form action="{{ route('store.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div>
                            
                            {{-- Reusable Input Component used here --}}
                            <x-reusablecomponent.input name="name"/>

                            <x-reusablecomponent.button>Save</x-reusablecomponent.button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-layout.master>