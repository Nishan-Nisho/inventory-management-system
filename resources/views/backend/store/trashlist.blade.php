<x-layout.master>
    <div class="row">
        <div class="col-10 offset-1">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('store.index') }}" class="btn btn-sm btn-primary float-right">Stores List</a>                
            </div>
    
            @if (session('message'))
                <p class="text-primary">{{ session('message') }}</p>
            @endif 
    
            <div class="card-body">
                
                    <p class="btn btn-danger form-control">Trashed List</p>
                        <table class="table table-hover  table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID#</th>
                                    <th class="text-center">store Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            @php
                                $i=1;
                            @endphp
                            <tbody>
                                @foreach ($store as $store)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td class="text-center">{{ $store->name }}</td>
                                    
                                    <td class="text-center">
                                        <a href="{{ route('store.restore',$store->id) }}" class="btn btn-sm btn-info">Restore</a>
                                        <a href="{{ route('store.permanentdelete',$store->id) }}" class="btn btn-sm btn-warning">Permanent Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </x-layout.master>