<x-layout.master>
    <div class="container">
        <div class="row">
            <div class="col-12">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('store.create') }}" class="btn btn-sm btn-primary float-start">Add store</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>--}}
                    <a href="{{ route('store.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash</a>
                    
                </div>
        
                @if (session('message'))
                    <p class="text-primary">{{ session('message') }}</p>
                @endif 
        
                <div class="card-body">
                    
                        
                            <table class="table table-hover  table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID#</th>
                                        <th class="text-center">Store Name</th>
                                        <th class="text-center">Boxes</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                @php
                                    $i=1;
                                @endphp
                                <tbody>
                                    @foreach ($store as $store)
                                    <tr>
                                        <td class="text-center">{{ $i++ }}</td>
                                        <td class="text-center">{{ $store->name }}</td>
                                        <td>
                                            @isset($store->boxes)
                                                @foreach ($store->boxes as $box)
                                                    <span class="badge badge-primary">{{ $box->name ?? '' }}</span>
                                                @endforeach
                                            @endisset

                                        </td>
                                                     
                                        
                                        <td class="text-center">
                                            <a href="{{ route('store.show',$store->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                            <a href="{{ route('store.edit',$store->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                            <a href="{{ route('store.delete',$store->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    
</x-layout.master>