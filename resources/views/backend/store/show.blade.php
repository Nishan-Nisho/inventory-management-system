<x-layout.master>
    <x-slot name=pageTitle>show store</x-slot>

<div class="row">
    <div class="col-8 offset-2">
     <a href="{{ route('store.index') }}" class="btn btn-sm btn-info mb-2 float-end">Back to store List</a>
     
     
     
     
     

     <table class="table table-striped table-bordered">
         <tr class="text-center">
             <th>Store Name</th>
         </tr>
 
         <tr class="text-center">
             <td class="text-center">{{ $store->name ?? ''}}</td>
             
         </tr>
     </table>
    </div>
</div>
</x-layout.master>

