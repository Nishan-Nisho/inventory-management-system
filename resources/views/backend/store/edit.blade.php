<x-layout.master>
    <x-slot name="pageTitle">Store Edit</x-slot>
<div class="row">
    <div class="col-8 offset-2">
        <div class="card bg-light">
            <div class="card-header">
                <a href="{{ route('store.index') }} " class="btn btn-sm btn-primary float-end">Store List</a>
            </div>

            <div class="card-body">
                <form action="{{ route('store.update',$store->id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    
                    <div>
                        <div>
                            <label for="">Store Name</label>
                        </div>
                        <input type="text" name="name" value="{{ $store->name }}" class="form-control">
                    </div>

                    {{-- Reusable Button Component Used here --}}
                    <x-reusablecomponent.button>Save</x-reusablecomponent.button>

                </form>
            </div>
        </div>
    </div>
</div>
</x-layout.master>