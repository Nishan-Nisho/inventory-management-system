<x-layout.master>
    <x-slot name="pageTitle">Customer Edit</x-slot>
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('customer.index') }} " class="btn btn-sm btn-primary float-end">Customer List</a>
                </div>
    
                <div class="card-body">
                            @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                            @endif
                             <form class="form-horizontal" action="{{ route('customer.update',$customer->id) }}" method="POST">
                                 @csrf
                                 <div>
                                    <div>
                                        <label class="col-md-4 control-label">Customer Name</label>
                                    </div>
                                    <input type="text" name="category_name" value="{{ $customer->name }}" class="form-control">
                                </div>

                                 <div>
                                   <div>
                                    <label class="col-md-4 control-label">Email</label>
                                   </div>
                                    <input type="text" name="email"  value="{{ $customer->email}}" class="form-control" />
                                </div>
                                 
                                <div>
                                    <div>
                                     <label class="col-md-4 control-label">Phone</label>
                                    </div>
                                     <input type="text" name="phone"  value="{{ $customer->phone}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Nid_No</label>
                                    </div>
                                     <input type="text" name="nid_no"  value="{{ $customer->nid_no}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Address</label>
                                    </div>
                                     <input type="text" name="address"  value="{{ $customer->address}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Gender</label>
                                    </div>
                                     <input type="text" name="gender"  value="{{ $customer->gender}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Date of Birth</label>
                                    </div>
                                     <input type="date" name="dob"  value="{{ $customer->dob}}" class="form-control" />
                                 </div>

                                 <div>
                                    <div>
                                     <label class="col-md-4 control-label">Remarks</label>
                                    </div>
                                     <input type="text" name="remarks"  value="{{ $customer->remarks}}" class="form-control" />
                                 </div>
                                 
                                <x-reusablecomponent.button>Update</x-reusablecomponent.button>
                             </form>
                            </div>
                        </div>
                    </div>
                </div>
 </x-layout.master>