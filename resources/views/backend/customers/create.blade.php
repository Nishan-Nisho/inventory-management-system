
<x-layout.master>
    <x-slot name="pageTitle">Customer Create</x-slot>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
        <div class="col-10 offset-1">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('customer.index') }}" class="btn btn-sm btn-primary float-end">Customers List</a>
                </div>

                <div class="card-body">
                    <form action="{{ route('customer.store') }}" method="POST">
                        @csrf
                            
                            {{-- Reusable Input Component used here --}}
                            <x-reusablecomponent.input name="name"/>
                            <x-reusablecomponent.input name="email"/>
                            <x-reusablecomponent.input name="phone"/>
                            {{-- <x-reusablecomponent.input name="nid_no"/> --}}
                            <x-reusablecomponent.input name="address"/>

                            <div class="form-group">
                                <div>
                                    <label>Gender</label>
                                </div>
                                <select class="form-control" name="gender">
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select> 
                            </div>

                            <div class="form-group">
                                <div>
                                    <label>Date_of_Birth</label>
                                </div>
                                <input type="date" name="dob" value="{{ old('dob') }}" class="form-control input-md" />
                            </div>

                            <x-reusablecomponent.input name="remarks"/>


                            <x-reusablecomponent.button>Save</x-reusablecomponent.button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-layout.master>