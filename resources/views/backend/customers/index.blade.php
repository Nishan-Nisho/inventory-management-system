<x-layout.master>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('customer.create') }}" class="btn btn-sm btn-primary float-start">Add Customer</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>--}}
                    <a href="{{ route('customer.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash List</a>
                    
                </div>
        
                @if (Session::has('message'))
                    <p class="alert alert-primary">{{ Session::get('message') }}</p>
                @endif 

                
        
                <div class="card-body">
                    
                    <table class="table table-hover table-bordered table-responsive">
                        <thead>
                            <tr>
                                <th class="text-center">Id</th>
                                <th class="text-center">Name</th>
                                <th class="text-center">email</th>
                                <th class="text-center">Phone</th>
                                <th class="text-center">Address</th>
                                <th class="text-center">Gender</th>
                                <th class="text-center">Date_of_Birth</th>
                                <th class="text-center">Remarks</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($customers as $customer )
                            <tr>
                                <td class="text-center">{{ $loop->iteration }}</td>
                                <td class="text-center">{{ $customer->name}}</td>
                                <td class="text-center">{{ $customer->email }}</td>
                                <td class="text-center">{{ $customer->phone }}</td>
                                <td class="text-center">{{ $customer->address }}</td>
                                <td class="text-center">{{ $customer->gender }}</td>
                                <td class="text-center">{{ $customer->dob }}</td>
                                <td class="text-center">{{ $customer->remarks }}</td>
                                <td class="text-center d-flex">
                                    <a href="{{ route('customer.show',$customer->id) }}" class="btn btn-sm btn-info me-1">Show</a>
                                    <a href="{{ route('customer.edit',$customer->id) }}" class="btn btn-sm btn-warning me-1">Edit</a>
                                    <a href="{{ route('customer.delete',$customer->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
</x-layout.master>

