
<x-layout.master>

    <x-slot name=pageTitle>show Customer</x-slot>

    

<div class="row">
    <div class="col-10 offset-1">
     <a href="{{ route('customer.index') }}" class="btn btn-sm btn-info mb-2 float-end">Back to Customers List</a>
     
     
     <table class="table table-striped table-bordered">
         <tr class="text-center">
            <th class="text-center">Name</th>
            <th class="text-center">email</th>
            <th class="text-center">Phone</th>
            <th class="text-center">Nid_No</th>
            <th class="text-center">Address</th>
            <th class="text-center">Gender</th>
            <th class="text-center">Date_of_Birth</th>
            <th class="text-center">Remarks</th>
         </tr>
 
         <tr class="text-center">
            <td class="text-center">{{ $customer->name}}</td>
            <td class="text-center">{{ $customer->email }}</td>
            <td class="text-center">{{ $customer->phone }}</td>
            <td class="text-center">{{ $customer->nid_no }}</td>
            <td class="text-center">{{ $customer->address }}</td>
            <td class="text-center">{{ $customer->gender }}</td>
            <td class="text-center">{{ $customer->dob }}</td>
            <td class="text-center">{{ $customer->remarks }}</td>
             
         </tr>
     </table>
    </div>
</div>
</x-layout.master>