
<x-layout.master>
    <div class="row">
        <div class="col-12 offset-sm">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('customer.index') }}" class="btn btn-sm btn-primary float-right">Customer List</a>                
            </div> 
    
            <div class="card-body">
                @if (Session::has('message'))
                <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
               @endif
                
                    <p class="btn btn-danger form-control">Trashed List</p>
                        <table class="table table-hover  table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">Id</th>
                                    <th class="text-center">Name</th>
                                    <th class="text-center">email</th>
                                    <th class="text-center">Phone</th>
                                    <th class="text-center">Nid_No</th>
                                    <th class="text-center">Address</th>
                                    <th class="text-center">Gender</th>
                                    <th class="text-center">Date_of_Birth</th>
                                    <th class="text-center">Remarks</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($customers as $customer )
                                <tr>
                                    <td class="text-center">{{ $loop->iteration }}</td>
                                    <td class="text-center">{{ $customer->name}}</td>
                                    <td class="text-center">{{ $customer->email }}</td>
                                    <td class="text-center">{{ $customer->phone }}</td>
                                    <td class="text-center">{{ $customer->nid_no }}</td>
                                    <td class="text-center">{{ $customer->address }}</td>
                                    <td class="text-center">{{ $customer->gender }}</td>
                                    <td class="text-center">{{ $customer->dob }}</td>
                                    <td class="text-center">{{ $customer->remarks }}</td>
                                    <td class="text-center d-flex">
                                        <a href="{{ route('customer.restore',$customer->id) }}" class="btn btn-primary">Restore</a>
                                        <a href="{{ route('customer.permanentdelete',$customer->id) }}" class="btn btn-warning">Delete</i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </x-layout.master>