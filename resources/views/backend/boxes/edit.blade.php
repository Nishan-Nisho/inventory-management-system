<x-layout.master>
    <x-slot name="pageTitle">Boxes Edit</x-slot>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('boxes.index') }} " class="btn btn-sm btn-primary float-end">Box List</a>
                </div>
    
                <div class="card-body">
                    <form action="{{ route('boxes.update',$box->id) }}" method="post" enctype="multipart/form-data">
                        @csrf

                        <div>
                            <div>
                                <label for="">Box name</label>
                            </div>
                            <input type="text" name="name" value="{{ $box->name }}" class="form-control">
                        </div>
    
                        {{-- Reusable Button Component Used here --}}
                        <x-reusablecomponent.button>Save</x-reusablecomponent.button>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</x-layout.master>