<x-layout.master>
    <div class="row">
        <div class="col-10 offset-1">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('boxes.index') }}" class="btn btn-sm btn-primary float-right">Boxes List</a>                
            </div>
    
            @if (session('message'))
                <p class="alert alert-danger text-danger">{{ session('message') }}</p>
            @endif 
    
            <div class="card-body">
                
                    <p class="btn btn-danger form-control">Trashed List</p>
                        <table class="table table-hover  table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID#</th>
                                    <th class="text-center">Box Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                           
                            <tbody>
                                @foreach ($boxes as $key=>$box)
                                <tr>
                                    <td class="text-center">{{ $key+1 }}</td>
                                    <td class="text-center">{{ $box->name }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('boxes.restore',$box->id) }}" class="btn btn-sm btn-info">Restore</a>
                                        <a href="{{ route('boxes.permanentdelete',$box->id) }}" class="btn btn-sm btn-warning">Permanent Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </x-layout.master>