<x-layout.master>
    <div class="container">
        <div class="row">
            <div class="col-12">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('boxes.create') }}" class="btn btn-sm btn-primary float-start">Make Box</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>
                    --}}
                    <a href="{{ route('boxes.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash List</a>
                    
                    
                </div>
        
                @if (session('message'))
                    <p class="alert alert-primary">{{ session('message') }}</p>
                @endif 
        
               
                <div class="card-body">
                    
                        
                    <table class="table table-hover  table-bordered">
                        <thead>
                            <tr>
                                <th class="text-center">ID#</th>
                                <th class="text-center">Box Name</th>
                                <th class="text-center">Store Name</th>
                                <th class="text-center">Action</th>
                            </tr>
                        </thead>
                        @php
                            $i=1;
                        @endphp
                        <tbody>
                            @foreach ($boxes as $box)
                            <tr>
                                <td class="text-center">{{ $i++ }}</td>
                                <td class="text-center">{{ $box->name }}</td>
                                <td class="text-center">{{ $box->store->name ?? '' }}</td>
                                
            
                                
                                <td class="text-center">
                                    <a href="{{ route('boxes.show',$box->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                    <a href="{{ route('boxes.edit',$box->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                    <a href="{{ route('boxes.delete',$box->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                </td> 
                            </tr>
                            @endforeach
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

            </div>
    </div>
</x-layout.master>