<x-layout.master>
    <x-slot name="pageTitle">Boxes Create</x-slot>
   
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="container">
        <div class="row">
        <div class="col-10 offset-1">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('boxes.index') }}" class="btn btn-sm btn-primary float-end">Boxes List</a>
                </div>

                <div class="card-body">
                    <form action="{{ route('boxes.store') }}" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div>
                            
                            {{-- Reusable Input Component used here --}}
                            <input type="text" name="name" class="form-control">

                            <div>
                                <label for="">Store Name</label>
                                <select name="store_id" id="" class="form-control">
                                    <option value="">Select Store</option>
                                    @foreach ($stores as $store)
                                        <option value="{{ $store->id }}">{{ $store->name ?? '' }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <x-reusablecomponent.button>Save</x-reusablecomponent.button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>
</x-layout.master>