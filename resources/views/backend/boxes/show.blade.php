<x-layout.master>

    <x-slot name="pageTitle">Boxes Show</x-slot>
    <div class="container">
        <div class="row">
            <div class="col-12">
             <a href="{{ route('boxes.index') }}" class="btn btn-sm btn-info mb-2 float-end">Boxes List</a>
             
             
             
             
             
     
             <table class="table table-striped table-bordered">
                 <tr class="text-center">
                    <th class="text-center">Box Name</th>
                 </tr>
         
                 <tr class="text-center">
                     <td class="text-center">{{ $box->name ?? ''}}</td>
                 </tr>
             </table>
            </div>
        </div>
    </div>
 
     {{-- <p>Name: {{ $color->colorName ?? '' }}</p>
     <p>Photo: {{ $color->colorImage ?? '' }}</p> --}}
 
</x-layout.master>