<x-layout.master>
    <div class="row">
        <div class="col-10 offset-1">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('roles.index') }}" class="btn btn-sm btn-primary float-right">Roles List</a>                
            </div> 
    
            <div class="card-body">
                
                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                @endif
                    <p class="btn btn-danger form-control">Trashed List</p>
                        <table class="table table-hover  table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID#</th>
                                    <th class="text-center">Category Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            @php
                                $i=1;
                            @endphp
                            <tbody>
                                @foreach ($roles as $role)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td class="text-center">{{ $role->name }}</td>
                                    
                                    <td class="text-center">
                                        <a href="{{ route('roles.restore',$role->id) }}" class="btn btn-sm btn-info">Restore</a>
                                        <a href="{{ route('roles.permanentdelete',$role->id) }}" class="btn btn-sm btn-warning">Permanent Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </x-layout.master>