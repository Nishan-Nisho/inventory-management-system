<x-layout.master>
    <x-slot name="pageTitle">Roles Edit</x-slot>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('roles.index') }} " class="btn btn-sm btn-primary float-end">Roles List</a>
                </div>
    
                <div class="card-body">
                    @if (Session::has('message'))
                        <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                    @endif
                    <form action="{{ route('roles.update',$role->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
            
                        <div>
                            <div>
                                <label for="">Role Name</label>
                            </div>
                            <input type="text" name="name" value="{{ $role->name }}" class="form-control">
                        </div>
    
                        {{-- Reusable Button Component Used here --}}
                        <x-reusablecomponent.button>Save</x-reusablecomponent.button>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</x-layout.master>