<x-layout.master>
    <div class="container">
        <div class="row">
            <div class="col-12">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('roles.create') }}" class="btn btn-sm btn-primary float-start">Add Roles</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>--}}
                    <a href="{{ route('roles.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash</a>
                    
                </div> 
        
                <div class="card-body">
                    @if (Session::has('message'))
                            <div class="alert alert-success" role="alert">{{Session::get('message')}}</div>
                            @endif
                        
                            <table class="table table-hover  table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID#</th>
                                        <th class="text-center">Roles Name</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                @php
                                    $i=1;
                                @endphp
                                <tbody>
                                    @foreach ($roles as $role)
                                    <tr>
                                        <td class="text-center">{{ $i++ }}</td>
                                        <td class="text-center">{{ $role->name }}</td>
                                        <td class="text-center">
                                            <a href="{{ route('roles.show',$role->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                            <a href="{{ route('roles.edit',$role->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                            <a href="{{ route('roles.delete',$role->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    
</x-layout.master>