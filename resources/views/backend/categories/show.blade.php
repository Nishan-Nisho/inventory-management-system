<x-layout.master>
    <x-slot name=pageTitle>show Categories</x-slot>

<div class="row">
    <div class="col-8 offset-2">
     <a href="{{ route('categories.index') }}" class="btn btn-sm btn-info mb-2 float-end">Back to Categories List</a>
     

     <table class="table table-striped table-bordered">
         <tr class="text-center">
             <th>Category Name</th>
         </tr>
 
         <tr class="text-center">
             <td class="text-center">{{ $category->category_name ?? ''}}</td>
             
         </tr>
     </table>
    </div>
</div>
</x-layout.master>