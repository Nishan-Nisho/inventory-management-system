<x-layout.master>
    <x-slot name="pageTitle">Categories Edit</x-slot>
<div class="container">
    <div class="row">
        <div class="col-8 offset-2">
            <div class="card bg-light">
                <div class="card-header">
                    <a href="{{ route('categories.index') }} " class="btn btn-sm btn-primary float-end">Categories List</a>
                </div>
    
                <div class="card-body">
                    <form action="{{ route('categories.update',$category->id) }}" method="post" enctype="multipart/form-data">
                        @csrf
                        
                        <div>
                            <div>
                                <label for="">Category Name</label>
                            </div>
                            <input type="text" name="category_name" value="{{ $category->category_name }}" class="form-control">
                        </div>
    
                        {{-- Reusable Button Component Used here --}}
                        <x-reusablecomponent.button>Save</x-reusablecomponent.button>
    
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</x-layout.master>