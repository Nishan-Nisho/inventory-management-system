<x-layout.master>
    <div class="row">
        <div class="col-10 offset-1">
        <div class="card bg-light">
            
            <div class="card-header">
                <a href="{{ route('categories.index') }}" class="btn btn-sm btn-primary float-right">Categories List</a>                
            </div>
    
            @if (session('message'))
                <p class="alert alert-danger text-danger">{{ session('message') }}</p>
            @endif 
    
            <div class="card-body">
                
                    <p class="btn btn-danger form-control">Trashed List</p>
                        <table class="table table-hover  table-bordered">
                            <thead>
                                <tr>
                                    <th class="text-center">ID#</th>
                                    <th class="text-center">Category Name</th>
                                    <th class="text-center">Action</th>
                                </tr>
                            </thead>
                            @php
                                $i=1;
                            @endphp
                            <tbody>
                                @foreach ($category as $category)
                                <tr>
                                    <td class="text-center">{{ $i++ }}</td>
                                    <td class="text-center">{{ $category->category_name }}</td>
                                    
                                    <td class="text-center">
                                        <a href="{{ route('categories.restore',$category->id) }}" class="btn btn-sm btn-info">Restore</a>
                                        <a href="{{ route('categories.permanentdelete',$category->id) }}" class="btn btn-sm btn-warning">Permanent Delete</a>
                                    </td>
                                </tr>
                                @endforeach
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    
    </x-layout.master>