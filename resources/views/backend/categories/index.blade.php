<x-layout.master>
    <div class="container">
        <div class="row">
            <div class="col-12">
            <div class="card bg-light">
                
                <div class="card-header">
                    <a href="{{ route('categories.create') }}" class="btn btn-sm btn-primary float-start">Add Categories</a>
                    {{-- <a href="{{ route('color.pdf') }}" class="btn btn-sm btn-success me-2 float-end">Make PDF</a>
                    <a href="{{ route('color.excel') }}" class="btn btn-sm btn-info me-2 float-end">Make Excel</a>--}}
                    <a href="{{ route('categories.trashlist') }}" class="btn btn-sm btn-warning me-2 float-end">Trash</a>
                    
                </div>
        
                @if (session('message'))
                    <p class="alert alert-primary">{{ session('message') }}</p>
                @endif 
        
                <div class="card-body">
                    
                        
                            <table class="table table-hover  table-bordered">
                                <thead>
                                    <tr>
                                        <th class="text-center">ID#</th>
                                        <th class="text-center">Category Name</th>
                                        <th class="text-center">Time</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                @php
                                    $i=1;
                                @endphp
                                <tbody>
                                    @foreach ($categories as $category)
                                    <tr>
                                        <td class="text-center">{{ $i++ }}</td>
                                        <td class="text-center">{{ $category->category_name }}</td>
                                        <td class="text-center">{{ $category->created_at }}</td>
                    
                                        
                                        <td class="text-center">
                                            <a href="{{ route('categories.show',$category->id) }}" class="btn btn-sm btn-info me-2">Show</a>
                                            <a href="{{ route('categories.edit',$category->id) }}" class="btn btn-sm btn-warning me-2">Edit</a>
                                            <a href="{{ route('categories.delete',$category->id) }}" class="btn btn-sm btn-danger">Delete</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    
</x-layout.master>