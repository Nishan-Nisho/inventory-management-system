<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">


        <!-- Styles -->
      

        <style>
            body {
                font-family: 'Nunito', sans-serif;
                background-color: rgb(35, 33, 50);
                margin-top: 20%;               
            }
            a{
                text-decoration: none;
            }
        </style>
    </head>
    <body class="antialiased">
        <div class="container text-center py-5">
             <div class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0">
            @if (Route::has('login'))
                <div>
                    @auth

                    <div class="container">
                <div class="h1 text-white text-opacity-50"> Hi.. Please visit <a class=" text-white" href="{{ url('/dashboard') }}">Dashboard</a>  </div>
                    
                    @else
                    <div class="container">
                <div class="h1 text-white text-opacity-50"> Hi.. Please <a class=" text-white" href="{{ url('/dashboard') }}">Log In</a>  to continue or <a class=" text-white" href="{{ route('register') }}">Register </a> to get started</div>
            </div>
                    @endauth
                </div>
            @endif

            

        
       
            </div>
         </div>

        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    </body>
</html>
