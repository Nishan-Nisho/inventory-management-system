<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return view('backend.roles.index',compact('roles'));
    }


    public function create()
    {
        return view('backend.roles.create');
    }
    

    public function store(Request $request)
    {
        try{
            Role::create([
                'name'=> $request->name ?? null
            ]);
            session()->flash('message','Data has stored successfully!');
            return redirect(route('roles.index'));

        }catch(Exception $e){
            dd($e->getMessage());
        }
        
    }

    public function show($id)
    {
        $role = Role::find($id);
       return view('backend.roles.show',compact('role'));
    }

    public function edit($id)
    {
        $role = Role::find($id);
        return view('backend.roles.edit',compact('role'));
    }

   
    public function update(Request $request, $id)
    {
        Role::find($id)->update([
            'name'=> $request->name ?? null
        ]);
        session()->flash('message','Data has updated successfully!');
        return redirect(route('roles.index'));
    }

    
    public function destroy($id)
    {
        $role = Role::find($id);
        $role->delete();
        session()->flash('message','Data has deleted successfully!');
        return redirect(route('roles.index'));

    }

    public function trashlist(){
        $roles=Role::onlyTrashed()->get();
        return view('backend.roles.trashlist',compact('roles'));
    }

    public function restore($id){
        $role=Role::onlyTrashed()->where('id', $id)->first();;
        $role->restore();
        session()->flash('message','Data has restore successfully!');
        return redirect(route('roles.trashlist'));
    }

    public function permanentDelete($id){
        $role=Role::onlyTrashed()->where('id', $id)->first();;
        $role->forceDelete();
        session()->flash('message','Data permanently Deleted From Database!');
        return redirect(route('roles.trashlist'));
    }
}
