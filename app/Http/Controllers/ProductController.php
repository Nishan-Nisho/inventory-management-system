<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\Box;
use App\Models\Store;
use App\Models\Vendor;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as Image;


class ProductController extends Controller
{
  
    public function index()
    {
        $products=Product::all();
        $stores = Store::all();
        return view('backend.inventory.index',compact('stores'));
        return view('backend.products.index',compact('products'));
    }

    
    public function create()
    {
        $categories = Category::all();
        $vendors= Vendor::all();
        $stores = Store::all();
        $boxes = Box::all();
        return view('backend.products.create', compact('categories','vendors','stores','boxes'));
    }

    public function store(Request $request)
    {
        

    //  try{
    //     Products::create([
    //         'category_id'=>$request->category_id,
    //         'vendor_id'=>$request->vendor_id,
    //         'name'=>$request->name,
    //         'description'=>$request->description,
    //         'image'=>$request->image,
    //         'buy_date'=>$request->buy_date,
    //         'unit_price'=>$request->unit_price,
    //         'selling_price'=>$request->selling_price,
    //         'quantity_sold'=>$request->quantity_sold,
    //         'status'=>$request->status,
    //         'remarks'=>$request->remarks
    //     ]);
    //     return redirect()->route('products.index')->withMessage('Products Data Saved Successfully');

    //  }catch(Exception $e){
    //     // echo $e->getMessage();
    //     return redirect()->route('products.create')->withErrors($e->getMessage());
    //  }
    
     try{
        // $data =$request->expect(['_token']);
        $data = $request->except(['_token']);
        if($request->hasFile('image')){
            $data['image']=$this->uploadImage($request->image,$request->name);
        }
        // dd($request->product_id);
        
        $newProduct = Product::create($data);
        $products = $request->product_id;
        $vendors = $request->vendor_id;

        // $productToUpdate = Product::where('id', $request->product_id)->first();
        $vendorToUpdate = Vendor::where('id', $request->vendor_id)->first();
        // dd($vendorToUpdate);

        // dd($newProduct);
        $newProduct->vendors()->attach([$request->vendor_id]);

        // $vendorToUpdate->product()->attach([$vendorToUpdate->id]);
        return redirect()->route('products.index')->withMessage('Product Information Saved Successfully');
    
    }catch(Exception $e){
        return redirect()->route('products.create')->withErrors($e->getMessage());
    }

    

    }
   
    public function show($id)
    {
        $product = Product::find($id);
        return view('backend.products.show',compact('product'));
    }

  
    public function edit($id)
    {
        $product= Product::find($id);
        return view('backend.products.edit',compact('product'));
    }

   
    public function update(Request $request, $id)
    {
        try{ 
            $product = Product::find($id);
            $data =$request->all();
            if($request->hasFile('image')){
                    
                    $destination ='storage/app/public/products/'.$product->image;
                    if(file_exists($destination)){
                        unlink($destination);
                    }
                    $data['image']=$this->uploadImage($request->image,$request->name);
                }
            $product->update($data);
            
        return redirect()->route('products.index')->withMessage('Data Updated Successfully');
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    
    public function delete($id)
    {
        
        $product=Product::find($id);
        $product->delete();
        return redirect()->route('products.index')->withMessage('Data Deleted Successfully');
    }

    // public function category(){
    //     $categories =Category::all();
    //     return view('backend.products.index',compact('categories'));
    // }


    private function uploadImage($file, $name){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        
        $file_name = $timestamp .'-'.$name. '.' .$file->getClientOriginalExtension();
        
        $pathToUpload = storage_path().'/app/public/products/';
        Image::make($file)->resize(200,250)->save($pathToUpload.$file_name);
        return $file_name;
    }

    
    
}
