<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Box;
use App\Models\Store;
use Illuminate\Http\Request;

class BoxController extends Controller
{
    public function index()
    {
        $boxes=Box::all();
        return view('backend.boxes.index',compact('boxes'));
    }

    public function create()
    {
        $stores = Store::all();
        return view('backend.boxes.create',compact('stores'));
    }

    
    public function store(Request $request)
    {
       
            $data = $request->all();
            Box::create($data);
            return redirect()->route('boxes.index');
    
    }
    
    public function show($id)
    {
        $box= Box::find($id);
        return view('backend.boxes.show',compact('box'));
    }

    public function edit($id)
    {
        $box= Box::find($id);
        return view('backend.boxes.edit',compact('box'));
    }

    public function update(Request $request, $id)
    {
        $box=Box::where('id',$id)->update([
            'name'=>$request->name,
        ]);
        return redirect()->route('boxes.index')->withMessage('Boxed Alter Successfully');
    }


    public function destroy($id)
    {
        $box=Box::find($id);
        $box->delete();
        return redirect()->route('boxes.index')->withMessage('Data Deleted Successfully');
    }

    public function trashlist(){
        $boxes=Box::onlyTrashed()->get();
        return view('backend.boxes.trashlist',compact('boxes'));
    }

    public function restore($id){
        $box=Box::onlyTrashed()->where('id', $id)->first();;
        $box->restore();
        return redirect()->route('boxes.trashlist')->withMessage('Restored Data Sucessfully');
    }

    public function permanentDelete($id){
        $box=Box::onlyTrashed()->where('id', $id)->first();;
        $box->forceDelete();
        return redirect()->route('boxes.trashlist')->withMessage('Data Deleted Permanently from Database!');
    }
}
