<?php

namespace App\Http\Controllers;

use Exception;
use Carbon\Carbon;
use App\Models\Order;
use App\Models\Product;
use App\Models\Customer;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    
    public function index()
    {
        $orders=Order::all();
        // dd($orders);
        return view('backend.orders.index',compact('orders'));
    }

    public function create()
    {
        $customers = Customer::all();
        $products = Product::all();
        return view('backend.orders.create',compact('customers','products'));
    }

    
    public function store(Request $request)
    {
        // dd($request->all());
        try{
            Order::create([
                'order_date'=>$request->order_date,
                'order_status'=>$request->order_status,
                'product_id' => $request->product_id,
                'customer_id'=> $request->customer_id,
                'quantity'=>$request->quantity,
                'unit_price'=>$request->unit_price,
                'total_price'=>$request->total_price,
                'order_date'=>$request->order_date
            ]);
            return redirect()->route('orders.index')->withMessage('Orders Data Saved Successfully');
    
         }catch(Exception $e){
        
            return redirect()->route('orders.create')->withErrors($e->getMessage());
         }
    }

    
    public function show($id)
    {
        $order= Order::find($id);
        $customer=Customer::find($id);
        return view('backend.orders.show',compact('order','customer'));
    }

    public function edit($id)
    {
        $order= Order::find($id);
        $customer= Customer::find($id);
        return view('backend.orders.edit',compact('order','customer'));
    }

    public function update(Request $request, $id)
    {
        Order::where('id',$id)->update([
            'order_date'=>$request->order_date,
            'order_status'=>$request->order_status,
            
            
        ]);
        return redirect()->route('orders.index')->withMessage('Orders Alter Successfully');
    }

    public static function boot()
    {
    Customer::boot();

    static::customer(function($order) {
        $order->order_date = \Carbon::now();
    });
    }
    // $model->update(['start_time' => Carbon::now()]);

    // static::customer(function($order) {
    //     $order->order_date = new Carbon();
        
    // });
    // }


    public function delete($id)
    {
        $order=Order::find($id);
        $order->delete();
        return redirect()->route('orders.index')->withMessage('Data Deleted Successfully');
    }

    public function customer(){
        $customers =Customer::all();
        return view('backend.orders.index',compact('customers'));
    }

    public function trashlist(){
        $customer = Customer::all();
        $order=Order::onlyTrashed()->get();
        return view('backend.orders.trashlist',compact('order','customer'));
    }

    public function restore($id){
        $order=Order::onlyTrashed()->where('id', $id)->first();;
        $order->restore();
        return redirect()->route('orders.trashlist')->withMessage('Restored Data Sucessfully');
    }

    public function permanentDelete($id){
        $order=Order::onlyTrashed()->where('id', $id)->first();;
        $order->forceDelete();
        return redirect()->route('orders.trashlist')->withMessage('Data Deleted Permanently from Database!');
    }
}
