<?php

namespace App\Http\Controllers;

use Image;
use Exception;

use App\Models\User;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;




class ProfileController extends Controller
{
    public function profileEdit($id){

        $user = User::find($id);
        
        return view('backend.users.profile', compact('user'));
     }

     public function profileUpdate(Request $request , $id){

        try{
            $user  = User::find($id);

            $user->update([
                'name' => $request->name,
            ]);
    
            $user->profile->update([
                'address' => $request->address,
                'phone_number' => $request->phone_number,
                'country' => $request->country,
                // 'image' => $request->image,
   
            ]);
        
            return redirect()->back();

        }catch(Exception $e ){
            dd($e->getMessage());
        }
       


    }

    


    public function changePassword(){

        
        
        return view('backend.users.change_password');

    }


    public function updatePassword(Request $request){

        

        $request->validate([
            'oldpassword' =>'required',
            'newpassword' =>'required|same:confirmpassword',
            'confirmpassword' =>'required|same:newpassword',
           
        ]);

    //    $hashedPassword = Auth::user()->newpassword;
    //    if(Hash::check($request->oldpassword,$hashedPassword)){
    //        $user = User::find(Auth::id());
    //        $user->newpassword = Hash::make($request->newpassword);
    //        $user->save();
    //     //    return redirect()->route('users.change_password')->withMessage('password Change Sucessfully');
    //     return redirect()->route();
    //    }

     $current_user = auth()->user();
     if(Hash::check($request->oldpassword,$current_user->password)){

        $current_user->update([
            'password' =>bcrypt($request->newpassword)
        ]);

        return redirect()->back();

     }
       


    }

        
        
   
       


    
    
 
}
