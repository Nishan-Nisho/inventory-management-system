<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    
    public function index()
    {
        $categories=Category::all();
        return view('backend.categories.index',compact('categories'));
    }

    
    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(Request $request)
    {
        

     try{
        Category::create([
            'category_name'=>$request->category_name,
            // 'created_at'=>$request->created_at,
        ]);
        return redirect()->route('categories.index')->withMessage('Categories Data Saved Successfully');

     }catch(Exception $e){
    
        return redirect()->route('categories.create')->withErrors($e->getMessage());
     }
    }

    public function show(Category $category,$id)
    {

        $category=Category::find($id);
        return view('backend.categories.show',compact('category'));
    }

    public function edit($id)
    {
        $category= Category::find($id);
        return view('backend.categories.edit',compact('category'));
    }

    public function update(Request $request, $id)
    {
        Category::where('id',$id)->update([
            'category_name'=>$request->category_name,
            // 'created_at'=>$request->created_at,
        ]);
        return redirect()->route('categories.index')->withMessage('Categories Updated Successfully');
   
    }

    public function delete($id)
    {
        $category=Category::find($id);
        $category->delete();
        return redirect()->route('categories.index')->withMessage('Data Deleted Successfully');
    }

    public function trashlist(){
        $category=Category::onlyTrashed()->get();
        return view('backend.categories.trashlist',compact('category'));
    }

    public function restore($id){
        $category=Category::onlyTrashed()->where('id', $id)->first();;
        $category->restore();
        return redirect()->route('categories.trashlist')->withMessage('Restored Data Sucessfully');
    }

    public function permanentDelete($id){
        $category=Category::onlyTrashed()->where('id', $id)->first();;
        $category->forceDelete();
        return redirect()->route('categories.trashlist')->withMessage('Data Deleted Permanently from Database!');
    }

    
}
