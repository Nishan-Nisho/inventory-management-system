<?php

namespace App\Http\Controllers;

use Exception;
use Image;
use Carbon\Carbon;
use App\Models\Vendor;
use Illuminate\Http\Request;

class VendorController extends Controller
{
    
    public function index(){
        $vendors= Vendor::all();
        return view('backend.vendors.index',compact('vendors'));
    }
    
    public function create()
    {
        $vendors = Vendor::all();
        return view('backend.vendors.create',compact('vendors'));
    }

    public function store(Request $request)
    {
        try{
            $data =$request->all();
            if($request->hasFile('image')){
                $data['image']=$this->uploadImage($request->image,$request->name);
            }
            
            Vendor::create($data);
            return redirect()->route('vendors.index')->withMessage('Vendors Information Saved Successfully');
        
        }catch(Exception $e){
            return redirect()->route('vendors.create')->withErrors($e->getMessage());
        }
    }

    public function show($id)
    {
        $vendor= Vendor::find($id);
        return view('backend.vendors.show',compact('vendor'));
    }

    public function edit($id)
    {
        $vendor= Vendor::find($id);
        return view('backend.vendors.edit',compact('vendor'));
    }

    
    public function update(Request $request, $id)
    {
        try{ 
            $vendor = Vendor::find($id);
            $data =$request->all();
            if($request->hasFile('image')){
                    
                    $destination ='storage/app/public/vendors/'.$vendor->image;
                    if(file_exists($destination)){
                        unlink($destination);
                    }
                    $data['image']=$this->uploadImage($request->image,$request->name);
                }
            $vendor->update($data);
            
        return redirect()->route('vendors.index')->withMessage('Data Updated Successfully');
        }catch(Exception $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        $vendor=Vendor::find($id);
        $vendor->delete();
        return redirect()->route('vendors.index')->withMessage('Data Deleted Successfully');
    }

    public function trashlist(){
        $vendor=Vendor::onlyTrashed()->get();
        return view('backend.vendors.trashlist',compact('vendor'));
    }

    public function restore($id){
        $vendor= Vendor::onlyTrashed()->where('id', $id)->first();;
        $vendor->restore();
        return redirect()->route('vendors.trashlist')->withMessage('Restored Data Sucessfully');
    }

    public function permanentDelete($id){

        $vendor= Vendor::onlyTrashed()->where('id', $id)->first();;
        $vendor->forceDelete();
        return redirect()->route('vendors.trashlist')->withMessage('Data Deleted Permanently from Database!');
                
    }
    private function uploadImage($file, $name){
        $timestamp = str_replace([' ',':'], '-', Carbon::now()->toDateTimeString());
        
        $file_name = $timestamp .'-'.$name. '.' .$file->getClientOriginalExtension();
        
        $pathToUpload = storage_path().'/app/public/vendors/';
        Image::make($file)->resize(200,250)->save($pathToUpload.$file_name);
        return $file_name;
    }
}
