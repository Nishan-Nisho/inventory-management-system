<?php

namespace App\Http\Controllers;

use Exception;
use App\Models\Box;
use App\Models\store;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function index()
    {
        $store = Store::all();
        return view('backend.store.index',compact('store'));
        
    }


    public function create()
    {
        return view('backend.store.create');
    }
    

    public function store(Request $request)
    {
        try{
            Store::create([
                'name'=> $request->name ?? null,
                
            ]);
            session()->flash('message','product has stored successfully!');
            return redirect(route('store.index'));

        }catch(Exception $e){
            dd($e->getMessage());
        }
        
    }

    public function show($id)
    {
        $store = Store::find($id);
       return view('backend.store.show',compact('store'));
    }

    public function edit($id)
    {
        $store = Store::find($id);
        return view('backend.store.edit',compact('store'));
    }

   
    public function update(Request $request, $id)
    {
        Store::find($id)->update([
            'name'=> $request->name ?? null,
            
        ]);
        session()->flash('message','Product has updated successfully!');
        return redirect(route('store.index'));
    }

    
    public function destroy($id)
    {
        $store = Store::find($id);
        $store->delete();
        session()->flash('message','store has deleted successfully!');
        return redirect(route('store.index'));

    }

    public function trashlist(){
        $store=Store::onlyTrashed()->get();
        return view('backend.store.trashlist',compact('store'));
    }

    public function restore($id){
        $store=Store::onlyTrashed()->where('id', $id)->first();;
        $store->restore();
        session()->flash('message','store has restore successfully!');
        return redirect(route('store.trashlist'));
    }

    public function permanentDelete($id){
        $store=Store::onlyTrashed()->where('id', $id)->first();;
        $store->forceDelete();
        session()->flash('message','Store Deleted From Database Permanently');
        return redirect(route('store.trashlist'));
    }
}
