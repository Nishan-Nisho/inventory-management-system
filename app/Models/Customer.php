<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Customer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name',
        'email',
        'phone',
        'nid_no',
        'address',
        'gender',
        'dob',
        'remarks',
    ];

    public function orders(){
        return $this->hasMany(Order::class,'customer_id','id');
    }
}
