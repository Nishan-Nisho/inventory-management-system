<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable=([
        'name',
        
    ]);


    // public function products(){
    //     return $this->hasMany(Product::class,'product_id','id');
    // }

    public function products(){
        return $this->hasMany(Product::class);
    }

    public function boxes(){
        return $this->hasMany(Box::class);
    }
}
