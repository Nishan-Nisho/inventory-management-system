<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Vendor extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable=([
        'name',
        'email',
        'phone',
        'address',
        'image',
        'remarks',
        'created_by',
        'update_by',
        'deleted_by',
    ]);

    public function products(){
        return $this->belongsToMany(Product::class);
       // return $this->belongsToMany(Product::class,'product_vendor','vendor_id','products_id');
    }
}
