<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $table = "products";

    
    // protected $fillable=([
    //     'category_id',
    //     'vendor_id',
    //     'store_id',
    //     'box
    //     -id',
    //     'name',
    //     'description',
    //     'image',
    //     'buy_date',
    //     'unit_price',
    //     'selling_price',
    //     'quantity_sold',
    //     'status',
    //     'remarks',
    //     'created_by',
    //     'updated_by',
    //     'deleted_by',
    // ]);

    protected $guarded = ['id'];

    public function category(){
        return $this->belongsTo(Category::class);
   }

   public function vendors(){
    return $this->belongsToMany(Vendor::class);
    //return $this->belongsToMany(Vendor::class, 'product_vendor', 'products_id','vendor_id');
    }

    public function store(){
        return $this->belongsTo(Store::class);
    }

    public function box(){
        return $this->belongsTo(Box::class);
    }

    public function orders(){
        return $this->belongsTo(Order::class,'order_id','id');
    }
}
