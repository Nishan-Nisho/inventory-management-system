<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Box extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable=[
        'name',
        'store_id'

    ];

    // public function product(){
    //     return $this->hasMany(Box::class,'box_id','id');
    // }

    public function product(){
        return $this->hasOne(Box::class);
    }

    public function store(){
        return $this->belongsTo(Store::class,'store_id','id');
   }
}
